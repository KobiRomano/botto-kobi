﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(itemPrerequisiteManager))]
public class PrerequisitsManagerEditor : Editor {



    private bool addFold = false;
    private ItemIndex addIndex = 0;
    private ItemIndex prerequisiteIndex;
   // private ItemCollationType addCollate = 0;
    private float addQuantity = 0f;
    private bool addCollecable = false;
    private string addDisplayName;
    private string prerequisiteDisplayName;
    


   // private List<bool> foldout;
 
    public override void OnInspectorGUI()
    {
        #region Setup
        itemPrerequisiteManager manager = (itemPrerequisiteManager)target;
        if(manager.getprerequisites()==null)
        {
            manager.setPrerequisites(new List<ItemPrerequisite>());
        }
        if (manager.foldout == null)
        {
            manager.foldout = new List<bool>(manager.Count);
        }

        #endregion

        #region Add Item
        addFold = EditorGUILayout.Foldout(addFold, "Add Item");
        if (addFold)
        {
            EditorGUILayout.BeginVertical("box");
            EditorGUILayout.LabelField("Item");
            addIndex = (ItemIndex)EditorGUILayout.EnumPopup("Item Code", addIndex);
            addDisplayName = EditorGUILayout.TextField("Display Name", addDisplayName);
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Prerequisite");
            EditorGUILayout.Space();
            prerequisiteIndex = (ItemIndex)EditorGUILayout.EnumPopup("Prerequisite Item", prerequisiteIndex);
            //addCollate = (ItemCollationType)EditorGUILayout.EnumPopup("Item Collate", addCollate);
            addQuantity = EditorGUILayout.FloatField("Prerequisite Quantity", addQuantity);
            addCollecable = EditorGUILayout.Toggle("Collecable", addCollecable);
            prerequisiteDisplayName = EditorGUILayout.TextField("Prerequisite Name", prerequisiteDisplayName);
            if (GUILayout.Button("Add Prerequisite"))
            {
                manager.addPrerequiite(addIndex, addDisplayName, prerequisiteIndex, prerequisiteDisplayName, addQuantity, addCollecable, null,null);
                Debug.Log("11");
                manager.foldout.Add(false);
            }
            EditorGUILayout.EndVertical();
        }
        #endregion

        #region Display Current
        for (int i = 0; i < manager.Count; i++)
        {
            #region setrup
            ItemPrerequisite pre = manager.getprerequisites()[i];
            Inventory reqs = pre.getInventory();
           if(reqs.inspectorCollapsed==null)
            {
                reqs.inspectorCollapsed = new List<bool>(reqs.Count);
            }
           while(reqs.inspectorCollapsed.Count<reqs.Count)
            {
                reqs.inspectorCollapsed.Add(false);
            }
            #endregion
           manager. foldout[i] = EditorGUILayout.Foldout(manager.foldout[i], pre._displayName);
            if (manager.foldout[i])
            {
                EditorGUILayout.BeginVertical("box");
                List<bool> collaspeList = reqs.inspectorCollapsed;
                List<InventoryItem> items = reqs.getList();
                if (GUILayout.Button("Remove Item"))
                {
                    manager.removeItem(pre._itemCode);
                }
                for (int j = 0; j < reqs.Count; j++)
                {
                    collaspeList[j] = EditorGUILayout.Foldout(collaspeList[j], items[j].ToString());

                    if (collaspeList[j])
                    {
                        EditorGUILayout.BeginVertical("box");
                        EditorGUILayout.LabelField("Item Code", items[j]._itemCode.ToString());
                        EditorGUILayout.LabelField("Collation Type", items[j]._collationType.ToString());
                        items[j]._quantity = EditorGUILayout.FloatField("Quantity", items[j]._quantity);
                        items[j]._collectable = EditorGUILayout.Toggle("Collecable", items[j]._collectable);
                        if (GUILayout.Button("Remove"))
                        {
                            reqs.remove(items[j]);
                            reqs.inspectorCollapsed.RemoveAt(j);
                        }
                        EditorGUILayout.EndVertical();
                    }
                }

                EditorGUILayout.EndVertical();
            }
        }

        #endregion


    }



}
