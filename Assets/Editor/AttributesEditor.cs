﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

[CustomEditor(typeof(GameAttributesSet))]
/// <summary>
/// custome editor for the <see cref="Attributes"/> object.
/// </summary>
public class AttributesEditor : Editor 
{
	/// <summary>
	/// The topcollapsed. controls the collapsed 
	/// state of the attribute list.
	/// </summary>
	private static bool Topcollapsed=false;
	/// <summary>
	/// The size of the attribute array.
	/// </summary>
	[SerializeField]
	private int size = 0;

	/// <summary>
	/// The attributes from the ntarget object.
	/// </summary>
	private GameAttributesSet attributes;

	/// <summary>
	/// Raises the inspector GU event.
	/// </summary>
	public override void OnInspectorGUI()
	{
		//bool Topcollapsed=true;

		GameAttribute[] newAttributes; // the new attribute array

		attributes = (GameAttributesSet)target; //the target object
		size = EditorGUILayout.IntField ("Size",attributes.getAttributes().Length);//gest the size of the attribute array from the user.
		//attributes.topCollator = EditorGUILayout.Toggle ("Top Collator", attributes.topCollator);
		Topcollapsed = EditorGUILayout.Foldout(Topcollapsed,"Attributes");//control the foldout state of the attribute list
		if (size != attributes.getAttributes().Length) {//resize the attribute arrays
			if (size >= 0) {
				bool[] oldCollapsed = attributes.inspectorCollapsed == null ? new bool[size] : attributes.inspectorCollapsed;
				attributes.inspectorCollapsed = new bool[size];
				//oldCollapsed.CopyTo(attributesCollapsed,0);
				GameAttribute[] old = attributes.getAttributes();
				attributes.resize(size);

				newAttributes = attributes.getAttributes();
				//Debug.Log("load array");
				for (int i=0; i<size; i++) {
					if (i < old.Length) {
						newAttributes [i] = old [i] == null ? new GameAttribute(GameAttributeCodes.EMPTY_ATTRIBUTE) : old [i];
						attributes.inspectorCollapsed [i] = oldCollapsed [i];
					} else {

						newAttributes [i] = new GameAttribute(GameAttributeCodes.EMPTY_ATTRIBUTE);
					}

				}
			} else { //rebuild empty arrays
				attributes.resize(0);
				newAttributes = attributes.getAttributes();
				attributes.inspectorCollapsed = new bool[0];
			}
			//Array.Sort (newAttributes);
		}
		if (Topcollapsed) //draws the attributes interface to the inspector
		{
//			Rect r = EditorGUILayout.BeginVertical();
			newAttributes = attributes.getAttributes();
			//Array.Sort(newAttributes);
			//size = EditorGUILayout.IntField ("Size",attributes._attributes.Length);
			for(int i=0;i<newAttributes.Length;i++)
			{
				//newAttributes[i] = EditorGUILayout.ObjectField(newAttributes[i]._code.ToString(),newAttributes[i],typeof(GameAttribute),false)as GameAttribute;
				GameAttribute att = newAttributes[i];
				EditorGUILayout.BeginVertical();
				attributes.inspectorCollapsed[i] = EditorGUILayout.Foldout(attributes.inspectorCollapsed[i], att.getCode().ToString() + " - " + att._floatValue + "-" + att._stringValue);
				if(attributes.inspectorCollapsed[i])
				{
//					switch (att._collateType)
//					{
//					case GameAttributeCollateType.floatAttribute:
//						att.setValue(EditorGUILayout.FloatField("value",att.getAttributeFloat()));
//						//Debug.Log("flotField");
//						break;
//					case GameAttributeCollateType.intAttribute:
//						att.setValue(EditorGUILayout.IntField("value",att.getAttributeInt()));
//						//Debug.Log("intfield");
//						break;
//						case game
//					default:
//						att.setValue(EditorGUILayout.TextField("value",att.getAttributeString()));
//						break;
//					}
					att.setValue(EditorGUILayout.TextField("String Value",att._stringValue));
					att.setValue(EditorGUILayout.FloatField("Numeric Value",att._floatValue));
					att.setCode((GameAttributeCodes)EditorGUILayout.EnumPopup ("Attribute Name", att.getCode()));
					att.setCode((GameAttributeCodes)EditorGUILayout.IntField ("Attribute Code", (int)att.getCode()));

					//att._stringValue =  EditorGUILayout.TextField ("Attribute Value", att._stringValue);
					EditorGUILayout.LabelField(att._collateType.ToString());
					att.collate = EditorGUILayout.Toggle("Collate",att.collate);
//					if(GUILayout.Button("Collate"))
//					{
//						Debug.Log("click");
//						attributes.collate(att.getCode());
//					}
					//EditorGUILayout.LabelField ("Attribute Name", AttributesIndex.getAttributeName (att._code));
					EditorGUILayout.Space();
				}
				EditorGUILayout.EndVertical();
			}

		}
	}

	//sorts the attribute array after the editor is destroyed
	void OnDestroy()
	{
		//Debug.Log ("Destroyed");
		//Array.Sort (attributes.getAttributes ());
	}


}
