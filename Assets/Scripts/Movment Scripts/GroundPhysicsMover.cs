﻿using UnityEngine;
using System.Collections;

public class GroundPhysicsMover : PhisicesMover {

	#region autoFind Members
	//private GroundCaster grounders;
	#endregion

	#region Inspector Controlled Members

	/// <summary>
	/// The sequential jump forces.
	/// </summary>
	public float[] jumpForces;
	/// <summary>
	/// indicates if horizontqal forces control is wllowed when not on grounded
	/// </summary>
	public bool airDrive=true;
	/// <summary>
	/// The ground sensor tollerance.
	/// <para>if the ground sensor returns values at least this close to 0 the object will be considered grounded</para>
	/// </summary>
	public float groundSensorTollerance;
	#endregion

	#region Private Members
	/// <summary>
	/// The jump phase.
	/// controlles which step of the jump forces will apply next
	/// </summary>
	private int jumpPhase=-1;
	/// <summary>
	/// The jump direction.
	/// </summary>
	private Vector3 jumpDirection;
//	/// <summary>
//	/// designates if the robot is in jump
//	/// </summary>
//	private bool isJumping=false;
	/// <summary>
	/// desinates if the objuct is on the ground.
	/// </summary>
	private bool isGrounded;
	#endregion








	void Start () 
	{
		init ();
	}

	protected override void init ()
	{
		base.init ();
		//grounders = GetComponentInChildren<GroundCaster> ();
	}

	public override void getCommands (CommandMask command)
	{
		horizontalKey = false;
		if(command.containsCommand(UserCommands.RIGHT_THRUST))
		{
			horizontalKey = true;
			if(isGrounded||airDrive)
			{
				if(!facingRight&&_rbody.velocity.x>-turnTollarance)
				{
					//Debug.Log("turn right");
					turnAround();
				}
				_rbody.AddForce(Vector3.right*forwardForce);
			}
		}
		else if(command.containsCommand(UserCommands.LEFT_THRUST))
		{
			horizontalKey = true;
			if(isGrounded||airDrive)
			{
				if(facingRight&&_rbody.velocity.x<turnTollarance)
				{
					//Debug.Log("turn left");
					turnAround();
				}
				_rbody.AddForce(Vector3.left*forwardForce);
			}
		}
		if(command.containsCommand(UserCommands.JUMP))
		{
			//Debug.Log("jump");
			if(isGrounded)
			{
				//isJumping=true;
				jumpPhase=0;
				jumpDirection=Vector3.up;
				_rbody.velocity += Vector3.up*0.01f;
			}
		}
	}



	void Update () 
	{
		float dtg= getDistanceToGround();
		isGrounded = Mathf.Abs(dtg)<groundSensorTollerance;
//		if (isGrounded) 
//		{
//			isJumping=false;
//		}

		//Debug.Log (Mathf.Abs( _rbody.velocity.x)<horizontalTollerance);
		if(Mathf.Abs( _rbody.velocity.x)<horizontalVelocityTollerance&&_rbody.velocity.x!=0f)
		{
			_rbody.velocity= new Vector3(0f,_rbody.velocity.y,0f);
			//Debug.Log("xStopped");
		}

		#region debug 
#if UNITY_EDITOR
		if(Input.GetKeyDown(KeyCode.L))
		{
			Debug.Log("dtg: "+getDistanceToGround());
			Debug.Log("grounded: " + isGrounded);
			Debug.Log("facing Right: "+facingRight);
			Debug.Log("rbody right: "+_rbody.gameObject.transform.right);
		}
#endif
		#endregion
	}

	void FixedUpdate()
	{
		//HDecay
//		if(_rbody.velocity.magnitude>0)
//		{
//			_rbody.velocity-=((new Vector3(_rbody.velocity.x,0,0)*(
//				(isGrounded ? horizontalDecay : airborneHDelay))));
//
//		}
		#region Vertical Movment
		if (jumpPhase>-1) {
			if (jumpPhase < jumpForces.Length) 
			{
				_rbody.AddForce (jumpDirection * jumpForces [jumpPhase]);
				//Debug.Log(jumpForces[jumpPhase]);
				jumpPhase++;
			}
			else
			{
				jumpPhase = -1;
			}
		} 
		#endregion
		if ((decayWhenPressed)||(!horizontalKey)) 
		{
			//Debug.Log("Decay");
			decayHorizontal ();

		}
		decayVertical ();
		limitVelocity ();
	}

}
