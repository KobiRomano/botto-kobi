﻿using UnityEngine;
using System.Collections;

public class GroundCaster : MonoBehaviour {

	public float castTolerance;
	public float offset;
	public bool debugMode;

	private Collider messuredCollider;

	void Start()
	{
		messuredCollider = GetComponent<Collider> ();
	}

	public bool isGrounded()
	{
		Ray ray = new Ray (transform.position + Vector3.right * offset, Vector3.down);
		RaycastHit hit;
		bool result;
		if (Physics.Raycast (ray,
		                   out hit,
		                   messuredCollider.bounds.extents.y + castTolerance)) {
			result=true;
		} 
		else 
		{
			result=false;
		}
		if (debugMode) 
		{
			Debug.DrawRay(ray.origin,ray.direction,result ? Color.green : Color.red);
		}
		return result;
	}
}
