﻿using UnityEngine;
using System.Collections;
using System;

[Flags]
 public enum UserCommands
{
    NIL = 0,
    RIGHT_THRUST = 1 << 1,
    LEFT_THRUST = 1 << 2,
    JUMP = 1 << 3,
    FIRE = 1 << 4,
    RELOAD = 1 << 5,
    ACTION = 1 << 6,
    MENU = 1 << 7,


    UPDATE_CALL = 1 << 29,
    FIXED_UPDATE_CALL = 1 << 30,
    KEY_UP = 1 << 31,
    KEY_DOWN = 1 << 32

}
