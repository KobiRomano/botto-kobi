﻿using UnityEngine;
using System.Collections;

public class Int3DData : MonoBehaviour {

    [HideInInspector]
    public int[] flatData;

    public int rows;
    public int cols;
    public int depth;

    public int getValue(int row,int col, int depth)
    {
        return flatData[row + (row * col) +(row * col * depth)];
    }
}
