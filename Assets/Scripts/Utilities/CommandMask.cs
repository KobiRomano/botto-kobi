﻿using UnityEngine;
using System.Collections;
using System;


public class CommandMask 

{
	private int maskValue=0;


	public CommandMask()
	{

	}


	public void addCommand(UserCommands newCommand)
	{
		maskValue =  maskValue | (int)newCommand;
	}
	
	public  void removeCommand(UserCommands command)
	{
		maskValue = maskValue & ~(int)command;
	}
	
	public  bool containsCommand(UserCommands Command)
	{
		int code =(int) Command;
		return (code & maskValue) == code;
	}

	public void reset()
	{
		maskValue = 0;
	}

	 public override string ToString ()
	{
		return string.Format ("Commands:"+maskValue);
	}
}
