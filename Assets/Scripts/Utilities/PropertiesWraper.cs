﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PropertiesWraper  {

    public enum DataTypes
    {
        FLOAT=1,
        BOOL=2,
        STRING=3
    }

    public string properyName;
    public string value;
    public DataTypes type;
}
