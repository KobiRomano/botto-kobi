﻿using UnityEngine;
using System.Collections;
using System;

public class EnableResponder : RiddleResponder
{
    public MonoBehaviour[] effectList;


    public override void respond(bool riddleSolved)
    {
        for (int i = 0;i< effectList.Length; i++)
        {
            effectList[i].enabled = riddleSolved;
        }

    }
}
