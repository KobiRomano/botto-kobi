﻿using UnityEngine;
using System.Collections;
using System;

public class ToggleRiddleActioner : RiddleAction
{

    Animator anim;

     void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public override void toggled()
    {
        toggleState = !toggleState;
        if(toggleState)
        {
            anim.SetTrigger("Drop");
            anim.SetLayerWeight(1, 1);
        }
        else
        {
            anim.SetTrigger("Rise");
            anim.SetLayerWeight(1, 0);  
        }
    }
}
