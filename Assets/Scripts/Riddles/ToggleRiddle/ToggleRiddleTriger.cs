﻿using UnityEngine;
using System.Collections;
using System;

public class ToggleRiddleTriger : RiddleTriger
{
    private bool entered=false;

    protected override void trigerActivation()
    {
        RiddleAction[] currentStage = getNextStage();
        
        
        for (int i = 0; i < currentStage.Length; i++)
        {
            currentStage[i].toggled();
        }
        nextStage++;
        manager.checkStatus();
        
    }

    internal override void OnTriggerEnter(Collider other)
    {
        if (!entered)
        {
            PlayerController.playerInteraction += trigerActivation;
            Debug.Log("Enter:" + gameObject.name);
            entered = true;
        }
    }

    internal override void OnTriggerExit(Collider other)
    {
        PlayerController.playerInteraction -= trigerActivation;
        Debug.Log("exit:" + gameObject.name);
        entered = false;
    }
}
