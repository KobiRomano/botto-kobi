﻿using UnityEngine;
using System.Collections;
using System;

public class ToggleRiddleManager : RiddleManager {

  

	
	

    public override void checkStatus()
    {
        bool result = true;
        stepCounter++;
        for (int i = 0;i< Actioners.Length;i++)
        {
            if(!Actioners[i].toggleState)
            {
                result = false;
                break;
            }
        }
        if (result)
        {
            riddleSolved();
        }
        else if(stepCounter%resetAfter==0)
        {
            resetRiddle();
        }

    }


    protected override void resetRiddle()
    {
        Debug.Log("reset");
        for(int i=0;i<Actioners.Length;i++)
        {
            
            if (Actioners[i].toggleState)
            {
                Actioners[i].toggled();
            }

        }
        for(int i=0;i< triggers.Length;i++)
        {

            triggers[i].restTrigger();

        }
    }

}
