﻿using UnityEngine;
using System.Collections;

public abstract class RiddleManager : MonoBehaviour
{

    public RiddleResponder[] responders;
    /// <summary>
    /// the <seealso cref="GameObject"/> containing the game actioners\n
    /// the array index corresponds to the actioner id.
    /// </summary>
    protected RiddleAction[] Actioners;

    /// <summary>
    /// the triggers in the riddle.
    /// </summary>
    protected RiddleTriger[] triggers;

    public int resetAfter = 2;

    protected int stepCounter;


    internal virtual void Awake()
    {
        Actioners = GetComponentsInChildren<RiddleAction>();
        triggers = GetComponentsInChildren<RiddleTriger>();
    }


    /// <summary>
    /// cheks the status of the riddle
    /// </summary>
    public abstract void checkStatus();


    /// <summary>
    /// called when the riddle is solved
    /// </summary>
    protected virtual void riddleSolved()
    {
        if (responders != null)
        {
            for (int i = 0; i < responders.Length; i++)
            {
                responders[i].respond(true);

            }
        }
        disableTriggers();
    }
    /// <summary>
    /// disables the trigers of the riddle;
    /// </summary>
    protected void disableTriggers()
    {
        for(int i=0;i<triggers.Length;i++)
        {
            triggers[i].disable();
        }
    }

    protected abstract void resetRiddle();
    

}
