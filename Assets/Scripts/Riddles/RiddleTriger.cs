﻿using UnityEngine;
using System.Collections;

public abstract class RiddleTriger : MonoBehaviour
{

    protected RiddleManager manager;
    //toggle stages
    public RiddleAction[] stage0;
    public RiddleAction[] stage1;
    public RiddleAction[] stage2;
    public RiddleAction[] stage3;
    public RiddleAction[] stage4;
    public RiddleAction[] stage5;
    public RiddleAction[] stage6;
    public RiddleAction[] stage7;
    /// <summary>
    /// the number of stages
    /// </summary>
    public int numOfStages;
    /// <summary>
    /// the stage to use on next trigger
    /// </summary>
    public int nextStage;

    internal virtual void Awake()
    {
        manager = GetComponentInParent<RiddleManager>();
    }

    internal abstract void OnTriggerEnter(Collider other);

    internal abstract void OnTriggerExit(Collider other);

    protected abstract void trigerActivation();

    public void disable()
    {
        PlayerController.playerInteraction -= trigerActivation;
        enabled = false;
    }

    public virtual void restTrigger()
    {
        nextStage = 0;
    }


    protected virtual RiddleAction[] getNextStage()
    {
        int stage = nextStage % numOfStages;
        switch (stage)
        {
            case 0:
                return  stage0;
              
            case 1:
                return stage1;
            
            case 2:
                return  stage2;
               
            case 3:
                return  stage3;
              
            case 4:
                return  stage4;
                
            case 5:
                return  stage5;
                
            case 6:
                return  stage6;
                
            case 7:
                return  stage7;
            default:
                return null;

        }
    }
}
