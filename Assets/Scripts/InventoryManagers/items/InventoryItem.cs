﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class InventoryItem
{
    #region inspector members



    /// <summary>
    /// The item code.
    /// </summary>
    public ItemIndex _itemCode;

	/// <summary>
	/// The type of the collation.
	/// </summary>
	public ItemCollationType _collationType;
	/// <summary>
	/// The quantity of the item in the inventory.
	/// </summary>
	public float _quantity;
	/// <summary>
	/// a flag indicating if this InventoryItem can be taken by other inventory
	/// </summary>
	public bool _collectable;

    public string _displayName;

    /// <summary>
    /// the image used when displaying this item in the UI.
    /// </summary>
    public Image _UIImage;
    /// <summary>
    /// the prefab used when this item is instantiated to the scene
    /// </summary>
    public GameObject _prefab;

//	/// <summary>
//	/// a flag indicating if the InventoryItem is enabled and can be used.
//	/// </summary>
//	public bool _enabled;

	#endregion

	#region Constructors
	public InventoryItem (ItemIndex code)
	{
		_itemCode = code;
	}
    public InventoryItem()
    {
        
    }

    public InventoryItem (ItemIndex code, ItemCollationType collation)
	{
		_itemCode = code;
		_collationType = collation;
	}

	public InventoryItem(ItemIndex code,
                            ItemCollationType collation,
                            float quantity,
                            bool collecable,
                            string name,
                            Image image,
                            GameObject prefab)
	{

		_itemCode = code;
		_collationType = collation;
		_quantity = quantity;
		_collectable = collecable;
        _displayName = name;
        _UIImage = image;
        _prefab = prefab;
	}

	public InventoryItem (InventoryItem other)
	{
		_itemCode = other._itemCode;
		_collationType = other._collationType;
		_quantity = other._quantity;
		_collectable = other._collectable;
        _displayName = other._displayName;
	}
	#endregion
	
     

	public override string ToString ()
	{
		return _itemCode.ToString () + "-" + _quantity;
	}

    public override bool Equals(object obj)
    {
        if(! (obj is InventoryItem))
        {
            return false;
        }
        InventoryItem otherItem = (InventoryItem)obj;
        if(_itemCode==otherItem._itemCode)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public override int GetHashCode()
    {
        return _itemCode.GetHashCode();
    }

    public string getDisplayName()
    {
        if (_displayName=="")
        {
            return _itemCode.ToString();
        }
        else
        {
            return _displayName;
        }
    }
}
