﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// this class represents a editable lis of prerequisites for inventory items
/// </summary>
public class itemPrerequisiteManager : MonoBehaviour
{

#if UNITY_EDITOR

    public List<bool> foldout;

    public List<ItemPrerequisite> getprerequisites()
    {
        return prerequisites;
    }

    public void setPrerequisites(List<ItemPrerequisite> newPrerequisites)
    {
        prerequisites = newPrerequisites;
    }


#endif

    internal class ItemPrerequisitesNameComparer : IComparer<ItemPrerequisite>
    {
        
        public int Compare(ItemPrerequisite x, ItemPrerequisite y)
        {
            return string.Compare(x._displayName, y._displayName);
        }
    }

    internal class ItemPrerequisitesNumberComparer : IComparer<ItemPrerequisite>
    {

        public int Compare(ItemPrerequisite x, ItemPrerequisite y)
        {
            
            if (x._itemCode > y._itemCode) return 1;
            if (x._itemCode < y._itemCode) return -1;
            return 0;
        }
    }

    /// <summary>
    /// The prerequisite list
    /// </summary>
    [SerializeField]
    private List<ItemPrerequisite> prerequisites;

    /// <summary>
    /// Sortes the prerequisites by item name;
    /// </summary>
    public void sortPrerequsitesByItemName()
    {
        sortPrerequisites(new ItemPrerequisitesNameComparer());
    }

    /// <summary>
    /// Sortes the prerequisites by item name;
    /// </summary>
    public void sortPrerequisitesByDisplayNames()
    {
        sortPrerequisites(new ItemPrerequisitesNumberComparer());
    }


    private void sortPrerequisites(IComparer<ItemPrerequisite> comparer)
    {
        prerequisites.Sort(comparer);
    }


    public ItemPrerequisite addItem(ItemIndex code,string displayName,bool collectable)
    {
        int index = findItemIndex(code);
        ItemPrerequisite result = null;
        if (index==-1)
        {
            result = new ItemPrerequisite(code, displayName, collectable,null,null);
            prerequisites.Add(result);
            return result;
            //sortPrerequisitesByDisplayNames();
        }
        return prerequisites[index];
    }

    public void addPrerequiite(ItemIndex code,
                                string itemName,
                                ItemIndex prerequisiteCode,
                                string prerequisiteName, 
                                float prerequisiteQuantity, 
                                bool collecable,
                                Image image,
                                GameObject prefab)
    {
        ItemPrerequisite preq = addItem(code, itemName, collecable);
        preq.addPrerequisite(prerequisiteCode,prerequisiteQuantity, prerequisiteName, collecable,image,prefab);
    }



    public void removeItem(ItemIndex code)
    {
        int index = findItemIndex(code);
        if(index!=-1)
        {
            prerequisites.RemoveAt(index);
        }
    }

    public int Count
    {
        get { return prerequisites.Count; }
    }


    private int findItemIndex(ItemIndex code)
    {
        for (int i = 0; i < prerequisites.Count; i++)
        {
            if (prerequisites[i]._itemCode == code)
            {
                return i;
            }
        }
        return -1;
    }

    public ItemPrerequisite getPrereauisite(InventoryItem item)
    {
        int index = findItemIndex(item._itemCode);
        if(index==-1)
        {
            return null;
        }
        return prerequisites[index];
    }

}
