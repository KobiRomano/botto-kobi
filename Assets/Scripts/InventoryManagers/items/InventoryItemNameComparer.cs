﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryItemNameComparer :IComparer<InventoryItem> {

    

        public int Compare(InventoryItem x, InventoryItem y)
        {
            return string.Compare(x._displayName, y._displayName);
        }
    
}
