﻿
using UnityEngine;
using System.Collections;

public enum ItemIndex  
{
	EmptyItem=0,
	//data
	Data_Normal=0001,

	//weapon plans
	Plans_Shutgun_1=1001,
	Plans_Shutgun_2=1002,
	Plans_Shutgun_3=1003,
	Plans_Grende_1=1101,
	Plans_Grende_2=1102,
	Plans_Grende_3=1103,
	Plans_Laser_1=1201,
	Plans_Laser_2=1202,
	Plans_Laser_3=1203,

	//thrust plans
	Plans_Hover_1=2001,
	Plans_Hover_2=2003,
	Plans_Hover_3=2003,
	Plans_Crawler_1=2101,
	Plans_Crawler_2=2102,
	Plans_Crawler_3=2103,
	Plans_Legs_1=2201,
	Plans_Legs_2=2202,
	Plans_Legs_3=2203


}
