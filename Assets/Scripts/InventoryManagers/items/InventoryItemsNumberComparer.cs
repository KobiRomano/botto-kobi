﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class InventoryItemsNumberComparer : IComparer<InventoryItem> {

    public int Compare(InventoryItem x, InventoryItem y)
    {

        if (x._itemCode > y._itemCode) return 1;
        if (x._itemCode < y._itemCode) return -1;
        return 0;
    }
}
