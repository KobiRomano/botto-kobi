﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
/// <summary>
/// tnis class represnts Prerequisite for crafting of inventory items. 
/// </summary>
public class ItemPrerequisite :InventoryItem 
{


	#if UNITY_EDITOR 
	[HideInInspector]
	public List<bool> inspectorCollapsed;

	public Inventory getInventory()
	{
		return prerequisites;
	}
	#endif
	/// <summary>
	/// The prerequisites for this item.
	/// </summary>
	private Inventory prerequisites;
	/// <summary>
	/// The iteration step. this is the next prerequisite that will be iterated
	/// </summary>
	private int iterationStep;

	/// <summary>
	/// Initializes a new instance of the <see cref="ItemPrerequisite"/> class.
	/// </summary>
	/// <param name="code">the ItemCode for which the prerequisites apply</param>
	public ItemPrerequisite (ItemIndex code,
                             string displayName,
                             bool collectable,
                             Image image,
                             GameObject prfab
                                ) : base (code,ItemCollationType.Multiple,0f,collectable,displayName,image,prfab)
	{
		prerequisites = new Inventory ();
        //_itemCode = code;
    }


    public ItemPrerequisite (InventoryItem item) : base (item)
    {
        prerequisites = new Inventory();
        _collationType = ItemCollationType.Multiple;
        _collectable = false;
       
    }



	
	/// <summary>
	/// Adds a prerequisite.\n
	/// Iteration will be reset after this call.
	/// </summary>
	/// <param name="item">The item to add s a prerequisite. Note the quantity is also factored when evaluating prerequisites</param>
	public void addPrerequisite(InventoryItem item)
	{
		item._collationType = ItemCollationType.SingularReplace;
		prerequisites.add (item);
		beginIterartion ();
	}

    public void addPrerequisite(ItemIndex code,float quantity,string displayName,bool collectable,Image image,GameObject prefab)
    {
        prerequisites.add( new InventoryItem(code, ItemCollationType.SingularReplace, quantity, collectable, displayName,image,prefab));

    }

    /// <summary>
    /// Removes the prerequisite./n
    /// Iteration will be reset after this call.
    /// </summary>
    /// <param name="item">Item.</param>
    public void removePrerequisite(InventoryItem item)
	{
		prerequisites.remove (item);
		beginIterartion ();
	}

	/// <summary>
	/// Begins the iterartion. after this call the next prerequisite is the first one.
	/// </summary>
	public void beginIterartion()
	{
		iterationStep = 0;
	}
	/// <summary>
	/// checks if the iteration has a next step.
	/// </summary>
	/// <returns><c>true</c>, if next was hased, <c>false</c> otherwise.</returns>
	public bool hasNext()
	{
		return iterationStep < prerequisites.Count;
	}

	/// <summary>
	/// Gets the next prerequisite in the iteration.
	/// </summary>
	/// <returns>The next prerequisite in the iteration. /n 
	/// if no more prerequisites exist returns null.</returns>
	public InventoryItem getNext()
	{
		if (hasNext ()) 
		{
			return new InventoryItem(prerequisites.lookAt(iterationStep));
		}
		return null;
	}

}
