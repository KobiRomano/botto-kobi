﻿using UnityEngine;
using System.Collections;

/// <summary>
/// this class is a monobehaviour facade for the <see cref="Inventory"/> class.
/// </summary>
public class InventoryComponent : MonoBehaviour 
{

#if UNITY_EDITOR 
	public Inventory getInventory()
	{
		return inventory;
	}
#endif

	[SerializeField]
	private bool collectable;
	/// <summary>
	/// A flag indicating if this inventory can be collected by other inventories.
	/// </summary>
	public bool Collectable
	{
		get { return collectable;}
		set {collectable = value;}
	} 

	[SerializeField]
	public Inventory inventory;

	void Start()
	{
        if (inventory == null)
        {
            inventory = new Inventory();
        }
	}


    public int indexOf(InventoryItem item)
    {
        return inventory.indexOf(item);
    }

    /// <summary>
    /// returns the item at the supplied index in the inventory
    /// </summary>
    /// <param name="index">the index of the item to return</param>
    /// <returns></returns>
    public InventoryItem lookAt(int index)
    {
        return inventory._items[index];
    }


    /// <summary>
    /// Add the specified item.
    /// </summary>
    /// <param name="item">Item.</param>
    public int add(InventoryItem item)
	{
		return inventory.add (item);
	}

	public bool checkItemPrerequisite(ItemPrerequisite prerequisites)
	{
		return inventory.checkItemPrerequisite (prerequisites);
	}

    /// <summary>
    /// The number of items in the inventory.
    /// </summary>
	public int Count
    {
        get
        {
            return inventory.Count;
        }

    }

	public void remove(InventoryItem item)
	{
		inventory.remove (item);
		
	}

	public void  collect(InventoryComponent other)
	{
		InventoryItem item;
		if (other == null || !other.enabled) 
		{
			return;
		}
		for (int i=0; i<other.Count; i++) 
		{
			item=other.inventory.lookAt(i);
			if (item._collectable)
			{
				add(item);
				item._collectable=false;
			}
		}
		switch (other.inventory.collectionBehavior) 
		{
		case InventoryBehaviors.Destroy:
			Destroy(other.gameObject);
			break;
		case InventoryBehaviors.Disable:
			other.enabled=false;
			break;
		default:
			break;
		}
	}

    /// <summary>
    /// Sortes the prerequisites by item name;
    /// </summary>
    public void sortInventoryByItemName()
    {
        inventory.sortInventoryByItemName();
    }

    /// <summary>
    /// Sortes the prerequisites by item name;
    /// </summary>
    public void sortInventoryByDisplayNames()
    {
        inventory.sortInventoryByDisplayNames();
    }

 
}
