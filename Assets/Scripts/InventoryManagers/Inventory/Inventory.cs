﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Inventory 
{
	#if UNITY_EDITOR 
    [SerializeField]
	public List<bool> inspectorCollapsed;
	
	public List<InventoryItem> getList()
	{
		return _items;
	}

	public void setList( List<InventoryItem> items)
	{
		_items = items;
	}


	
	#endif

	#region public members

	/// <summary>
	/// The collection behavior. this detemines what happens to the inventory after collection.
	/// </summary>
	public InventoryBehaviors collectionBehavior;
	#endregion

	#region Private Members
	[SerializeField]
	public List<InventoryItem> _items;


    #endregion

    public Inventory()
    {
        if (_items == null)
        {
            _items = new List<InventoryItem>();
        }
    }


	/// <summary>
	/// Add the specified item. 
	/// <para>The item's collate type will determine the addition style</para>
	/// </summary>
	/// <param name="item">Item.</param>
	public int add(InventoryItem item)
	{
        int index = findItemIndex(item._itemCode);

		if (index > -1) {
			switch (_items[index]._collationType) {
			case ItemCollationType.Multiple:
				_items.Add (item);
				index = _items.Count-1;
				break;
			case ItemCollationType.SingularReplace:
				_items.RemoveAt (index);
				_items.Add (item);
				index = _items.Count-1; 
				break;
			case ItemCollationType.SingularRetaine:
				break;
			case ItemCollationType.SingularAccumulate:
				_items [index]._quantity += item._quantity;
				break;
			default:
				break;
			}

		} 
		else 
		{
			_items.Add(item);
			index = _items.Count-1;
		}
		return index;
	}

    private int findItemIndex(ItemIndex code)
    {
        for (int i = 0; i < _items.Count; i++)
        {
            if (_items[i]._itemCode == code)
            {
                return i;
            }
        }
        return -1;
    }

    /// <summary>
    /// return the inventory item with the given index
    /// </summary>
    /// <returns>The <see cref="InventoryItem"/>.</returns>
    /// <param name="index">the inventory item coresponding Index./n if Index is illegal null will be returned</param>
    public InventoryItem lookAt(int index)
	{
		if (index > _items.Count - 1)
			return null;
		//return new InventoryItem (_items [index]);
		return _items [index];
	}

    public int indexOf(InventoryItem item)
    {
        return findItemIndex(item._itemCode);
    }

	/// <summary>
	/// Remove the specified item from the inventory.
	/// <para>this will remove the first instance of the item in the inventory</para>
	/// </summary>
	/// <param name="item">Item.</param>
	public void remove(InventoryItem item)
	{
		_items.RemoveAt (findItemIndex( item._itemCode));

	}
	
    /// <summary>
    /// The number of items in the inventory.
    /// </summary>
    public int Count
    {
        get
        {
            return _items.Count;
        }
    }

	/// <summary>
	/// Checks if this inventory contains a given prerequisites 
	/// </summary>
	/// <returns><c>true</c>, if the inventory contains the prerequisites <c>false</c> otherwise.</returns>
	/// <param name="prerequisite">Prerequisite to match to the inventory.</param>
	public bool checkItemPrerequisite(ItemPrerequisite prerequisites)
	{
		bool result = true;
		prerequisites.beginIterartion ();
		InventoryItem prerequsite = prerequisites.getNext ();
		while (prerequsite!=null && result) 
		{
			result=checkPrerequisiteItem(prerequsite);
		}
		return result;
	}

    /// <summary>
    /// Sortes the prerequisites by item name;
    /// </summary>
    public void sortInventoryByItemName()
    {
        sortInventory(new InventoryItemNameComparer());
    }

    /// <summary>
    /// Sortes the prerequisites by item name;
    /// </summary>
    public void sortInventoryByDisplayNames()
    {
        sortInventory(new InventoryItemsNumberComparer());
    }

  


    private void sortInventory(IComparer<InventoryItem> comparer)
    {
        _items.Sort(comparer);
    }

    private bool checkPrerequisiteItem(InventoryItem prerequisiteItem)
	{
		for (int i=0; i<_items.Count; i++) 
		{
			InventoryItem item= _items[i];
			if(item._itemCode == prerequisiteItem._itemCode)
			{
				if(prerequisiteItem._quantity<=0&&item._quantity==-prerequisiteItem._quantity)
				{
					return true;
				}
				else
				{
					if(item._quantity>=prerequisiteItem._quantity)
					{
						return true;
					}
				}
			}
		}
		return false;

	}
}
