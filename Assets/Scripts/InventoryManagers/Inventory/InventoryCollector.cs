﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Inventory collector.
/// theis class checks for triggers activation and attemps to collect inventories when possible.
/// </summary>
public class InventoryCollector : MonoBehaviour {

	#region Priate Members
	[SerializeField]
	private InventoryComponent _inventory;
	#endregion

	void OnTriggerEnter(Collider other)
	{
		InventoryComponent colleced = other.GetComponent<InventoryComponent> ();
		if (colleced != null && colleced.Collectable) 
		{
			_inventory.collect(colleced);
		}
	}


}
