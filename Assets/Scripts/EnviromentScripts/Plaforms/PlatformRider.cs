﻿using UnityEngine;
using System.Collections;

public class PlatformRider : MonoBehaviour {

    [HideInInspector]
    private Transform father;

    void Start()
    {
        father = transform.parent;
       // Debug.Log("parent:" + father.name);
    }

    public void transferParent(Transform newParent)
    {
        
        transform.parent = newParent;
    }

    public void restParent()
    {
        transform.parent = father;
    }

}
