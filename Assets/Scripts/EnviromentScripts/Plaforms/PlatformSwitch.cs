﻿using UnityEngine;
using System.Collections;
using System;

public class PlatformSwitch : MonoBehaviour {

    private bool entered = false;

    public PlatformController platform;

     void OnTriggerEnter(Collider other)
    {
        if (enabled && !entered)
        {
            PlayerController.playerInteraction += trigerActivation;
            Debug.Log("Enter:" + gameObject.name);
            entered = true;
        }
    }


    void OnTriggerExit(Collider other)
    {
        PlayerController.playerInteraction -= trigerActivation;
        Debug.Log("exit:" + gameObject.name);
        entered = false;
    }

    private void trigerActivation()
    {
        if (enabled)
        {
            platform.Playing = true;
        }
    }

    void Update()
    {

    }
}
