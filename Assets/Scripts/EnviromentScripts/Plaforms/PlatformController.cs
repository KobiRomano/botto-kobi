﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Controller for moving platforms in the game
/// </summary>
public class PlatformController : MonoBehaviour {

    /// <summary>
    /// the movment velocity of the platform.
    /// if way points are used this vector size will determine the speed of movment
    /// </summary>
    public Vector3 velocity;

    /// <summary>
    /// waypoints to move throught.
    /// </summary>
    public Vector3[] wayPoints;


    /// <summary>
    /// determines if the platform will move
    /// </summary>
    public bool Playing;
    /// <summary>
    /// determine if the waypoints will loop back when the last one is reached.
    /// </summary>
    public bool loopWaypoints = true;
    /// <summary>
    /// The triger that collects  passengers
    /// </summary>
    public Collider collectionTrigger;




    private int nextWayPoint=0;
    

    private const float crossRadius = .5f;

    
	// Use this for initialization
	void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Playing)
        {
            movePlatform();
        }
	}

    /// <summary>
    /// Moves the platform.
    /// </summary>
    private void movePlatform()
    {
        if (wayPoints.Length == 0)
        {
            transform.Translate(velocity * Time.deltaTime);
        }
        else
        {
            if(transform.position.Equals(wayPoints[nextWayPoint%wayPoints.Length]))//waypoint reached
            {
                nextWayPoint++;
                if(nextWayPoint%wayPoints.Length==-0)//looping check
                {
                    Playing = loopWaypoints;
                }
            }
            else
            {
                Vector3 movmentVector = (wayPoints[nextWayPoint % wayPoints.Length]- transform.position).normalized*velocity.magnitude*Time.deltaTime;
                if(Vector3.Distance(transform.position, wayPoints[nextWayPoint % wayPoints.Length])<movmentVector.magnitude)
                {
                    transform.position = wayPoints[nextWayPoint % wayPoints.Length];
                }
                else
                {
                    transform.Translate(movmentVector);
                }
            }
        }
    }




    void OnDrawGizmosSelected()
    {
        if (wayPoints != null)
        {
            for(int i=0;i<wayPoints.Length;i++)
            {
                drawCross(wayPoints[i]);
            }
        }

    }

    /// <summary>
    /// draws a cross on the inspector gui in a worldspace point
    /// </summary>
    /// <param name="point">The position in worldspace of the cross center</param>
    private void drawCross(Vector3 point)
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(point + Vector3.up * crossRadius, point - Vector3.up * crossRadius);
        Gizmos.DrawLine(point + Vector3.right * crossRadius, point - Vector3.right * crossRadius);
    }

}
