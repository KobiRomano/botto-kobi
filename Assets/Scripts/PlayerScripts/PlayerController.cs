﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerController : MonoBehaviour {

    public delegate void PlayerInteract();
    public static event PlayerInteract playerInteraction;

	[SerializeField]
	private PhisicesMover thruster;
    [SerializeField]
    private Weapon weapon;

    

	#region key binding
	public KeyCode rightKey;
	public KeyCode leftKey;
	public KeyCode upKey;
    public KeyCode fire;
    public KeyCode reload;
    public KeyCode interact;
	#endregion

	private CommandMask thrustCommands;
    private CommandMask weaponCommands;

	// Use this for initialization
	void Start () 
	{
		thruster = GetComponentInChildren<PhisicesMover> ();
		thrustCommands = new CommandMask();
        weapon = GetComponentInChildren<Weapon>();
        weaponCommands = new CommandMask();
	}
	
    void Update()
    {
        weaponCommands.reset();
        weaponCommands.addCommand(UserCommands.UPDATE_CALL);
        if(Input.GetKey(fire))
        {
            weaponCommands.addCommand(UserCommands.FIRE);
        }
        if(Input.GetKeyDown(reload))
        {
            weaponCommands.addCommand(UserCommands.RELOAD);
        }
        if(Input.GetKeyDown(interact))
        {
            if (playerInteraction != null)
            {
                playerInteraction.Invoke();
            }
        }
        weapon.getCommands(weaponCommands);
    }

	void FixedUpdate()
	{
		thrustCommands.reset();
		thrustCommands.addCommand (UserCommands.FIXED_UPDATE_CALL);
		if(Input.GetKey(rightKey))
		{
			thrustCommands.addCommand(UserCommands.RIGHT_THRUST);

		}
		if(Input.GetKey(leftKey))
		{
			thrustCommands.addCommand(UserCommands.LEFT_THRUST);
		}
		if (Input.GetKey (upKey)) 
		{
			thrustCommands.addCommand(UserCommands.JUMP);
		}
		//Debug.Log (thrustCommands);
		thruster.getCommands (thrustCommands);
	}

        
}
