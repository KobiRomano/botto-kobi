﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(InventoryComponent))]
public class InventoryItemListbox : MonoBehaviour {

    public delegate void ValueChange(int index);
    public event ValueChange OnValueChange;


    private int _currentSelection=-1;
    /// <summary>
    /// the currently selected item in the box;
    /// </summary>
    [HideInInspector]
    public InventoryItem CurrentSelection
    {
        get
        {
            if (_currentSelection < 0) return null;
            return new InventoryItem(_Items.lookAt(_currentSelection));
        }
    }

    /// <summary>
    /// the items in the list box.
    /// </summary>
    private InventoryComponent _Items;

    /// <summary>
    /// a prefab for a button in the list.
    /// </summary>
   public GameObject ListButton;
    /// <summary>
    /// the parent objet for the item buttons of the list.
    /// </summary>
    public GameObject ListWraper;
    /// <summary>
    /// a prefab for the list scroll bar.
    /// </summary>
   public Scrollbar scrollBar;

    

    public bool alwaysOneSelected = true;


    void Awake()
    {
        _Items = GetComponent<InventoryComponent>();
        
    }

    private void CleanListWraper()
    {
        ListboxButton[] buttons = ListWraper.GetComponentsInChildren<ListboxButton>();
        
        for (int i=0;i<buttons.Length; i++)
        {
            buttons[i].unRegister();
            Destroy(buttons[i].gameObject);
        }
    }

    public void fillListWraper()
    {
        CleanListWraper();
        int vOffset = 0;
        RectTransform wraper = ListWraper.GetComponent<RectTransform>();
        wraper.sizeDelta = new Vector2(wraper.sizeDelta.x, ListButton.GetComponent<RectTransform>().sizeDelta.y * _Items.Count);
        for (int i=0;i<_Items.Count;i++)
        {
            GameObject newButton = Instantiate(ListButton);

            
            
            newButton.transform.SetParent(ListWraper.transform);
            RectTransform rtransform = newButton.GetComponent<RectTransform>();
            rtransform.offsetMax = new Vector2(0, rtransform.sizeDelta.y );
            rtransform.offsetMin = Vector2.zero;
            rtransform.anchoredPosition = new Vector2(0, rtransform.sizeDelta.y * -vOffset - rtransform.sizeDelta.y / 2);
            vOffset++;

            InventoryItem item = _Items.lookAt(i);
            ListboxButton listButton = newButton.GetComponent<ListboxButton>();
            listButton.setText(item.getDisplayName());
            listButton.ValueIndex = i;
        }
        GetComponentInParent<ScrollRect>().verticalNormalizedPosition = 1f;
    }

    public void setValue(int index)
    {
        if (index > -1 && index < _Items.Count)
        {
            if (index == _currentSelection)
            {
                if (!alwaysOneSelected)
                {
                    _currentSelection = -1;
                    OnValueChange.Invoke(_currentSelection);
                }
            }
            else
            {
                _currentSelection = index;
                OnValueChange.Invoke(_currentSelection);
            }
        }
    }

    public void setValue(InventoryItem item)
    {
        setValue(_Items.indexOf(item));
    }

    
}
