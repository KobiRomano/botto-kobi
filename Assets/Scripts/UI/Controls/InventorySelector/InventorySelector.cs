﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(InventoryComponent))]
public abstract class InventorySelector : MonoBehaviour {

    public enum SelectorGenerationType
    {
        ALL=0,
        PREREQUISITE=1,
        EXISTS=2
    }

    #region inspector members
    public itemPrerequisiteManager prerequisites;
    public SelectorGenerationType generationType;
    public bool forceNull;
    #endregion
    /// <summary>
    /// the currentlly active set of the selector
    /// </summary>
    protected Inventory activeSet;
    /// <summary>
    /// the complete set of items available to the selector.
    /// </summary>
    protected InventoryComponent fullSet;
    /// <summary>
    /// the plyers current inventory
    /// </summary>
    protected InventoryComponent playerInventory;
    /// <summary>
    /// the currently selected item.
    /// </summary>
    protected InventoryItem currentSelection;

    void Awake()
    {
        fullSet = GetComponent<InventoryComponent>();
        fullSet.sortInventoryByItemName();
        playerInventory = GameObject.FindGameObjectWithTag(TagsIndex.Player.ToString()).GetComponent<InventoryComponent>();
    }


    protected abstract void generateActiveSet();
  
}
