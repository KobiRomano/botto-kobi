﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Weapon : MonoBehaviour
{

    #region base values
    public float AmmoCapacity;
    public float recoilForce;
    public float range;
    public float damage;
    public float reloadTime;
    public float ammoClipCapacity;
    public float fireRate;
    #endregion

    #region tracking values
    public float CurrentAmmo;
    public float CurrentClipAmmo;
    #endregion

    #region settings
    public AimController aim;
    public bool continuousFire;
    #endregion

    internal float reloadTimer;
    internal bool reloading;
    internal float fireTimer;

    internal bool fired = false;

    protected int shotID = 0;

    public int ShotID
    {
        get
        {
            return shotID;
        }
    }
    /// <summary>
    /// fires the weapon
    /// </summary>
    internal abstract void fire();
    

    internal virtual void reload()
    {
        if (CurrentAmmo>0)
        {
            if(CurrentAmmo>ammoClipCapacity - CurrentClipAmmo)
            {
                CurrentAmmo -= ammoClipCapacity - CurrentClipAmmo;
                CurrentClipAmmo = ammoClipCapacity;

            }
            else
            {
                CurrentClipAmmo += CurrentAmmo;
                CurrentAmmo = 0f;
            }
        }
    }

    internal virtual bool canFire()
    {
        if (continuousFire || !fired)
        {
            if (CurrentClipAmmo > 0 && fireTimer <= 0)
            {
                return true;
            }
        }
        return false;

    }

    internal virtual void getCommands(CommandMask commands)
    {
        if(commands.containsCommand(UserCommands.FIRE))
        {
            if(continuousFire||(!fired))
            {
                fire();
                fired = true;
            }
        }

        else
        {
            fired = false;
        }
        if(commands.containsCommand(UserCommands.RELOAD))
        {
            reload();
        }
    }

}
