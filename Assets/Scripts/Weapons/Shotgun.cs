﻿using UnityEngine;
using System.Collections;
using System;

using System.Linq;


public class Shotgun : Weapon {

    public int coneAngle;
    public int coneDensity;


    

    private ParticleSystem shotVisual;
   
    void Start()
    {

        shotVisual = GetComponent<ParticleSystem>();
      
    }

    internal override void fire()
    {
        if(canFire())
        {
            
            
            //GameObject[] hits = CustomeCasters.ConeCast2D(transform.position, aim.getMouseWorldPosition()-aim.transform.position ,coneAngle, range, coneDensity,int.MaxValue);
            //HitResponder[] targets = new HitResponder[hits.Length];
            //int j = -1;
            //for (int i = 0;i<hits.Length;i++)
            //{
            //    HitResponder hr = hits[i].GetComponentInParent<HitResponder>();
            //    if(hr!=null)
            //    {
            //        j++;
            //        targets[j] = hr;
            //    }
            //}
            // targets = targets.Distinct(new HitResponder.HitRespondEquator()).ToArray();
            //HitEffect shotEffect = new HitEffect(HitEffectIndex.DAMAGE, damage);
            //for (int i = 0; i < targets.Length; i++)
            //{
            //    if(targets[i]!=null)
            //    {
            //        targets[i].attributeHit(shotEffect);
            //    }
            //}


            fireTimer = 1 / fireRate;
            fired = true;
            shotVisual.Emit(10);
            shotID++;
        }
    }

    void Update()
    {
        if (fireTimer > 0)
        {
            fireTimer -= Time.deltaTime;
        }
    }

}
