﻿using UnityEngine;
using System.Collections;
using System;

public class PaticalHitResponder : HitResponder
{
    int lastHitID = 0;

    private GameAttributeManager attributeManager;

    void Awake()
    {
        attributeManager = GetComponentInParent<GameAttributeManager>();
    }

    public override void attributeHit(HitEffect hitEffect)
    {
        attributeManager.modifiyAttributeValue(GameAttributeCodes.CURRENT_HP, hitEffect.Value);
    }

    void OnParticleCollision(GameObject other)
    {
        if(other.tag == TagsIndex.Weapon)
        {
            Weapon weapon = other.GetComponent<Weapon>();
            if (weapon != null && lastHitID!=weapon.ShotID)
            {
                attributeHit(new HitEffect(HitEffectIndex.DAMAGE, weapon.damage));
                lastHitID = weapon.ShotID;
            }
            Debug.Log("hit by:" + other.name + " for " + attributeManager.getAttributeiFloat(GameAttributeCodes.CURRENT_HP) + " id:" + lastHitID);
        }
    }
}
