﻿using UnityEngine;
using System.Collections;
using System;

public class StandardHitResponder : HitResponder
{
    private GameAttributeManager attributeManager;

    void Awake ()
    {
        attributeManager = GetComponentInParent<GameAttributeManager>();
    }


    public override void attributeHit(HitEffect hitEffect)
    {
        Debug.Log(gameObject.name);
        attributeManager.modifiyAttributeValue(GameAttributeCodes.CURRENT_HP, hitEffect.Value);
    }


}
