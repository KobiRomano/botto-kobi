﻿using UnityEngine;
using System.Collections;

public enum GameAttributeCodes  
{

	EMPTY_ATTRIBUTE=0,

	CURRENT_HP=1,
	MAX_HP=2,
	MIN_HP=3,
	
	
	CURRENT_SPEED=10,
	MAX_SPEED=11,
	ACCELERATION=12,
	DECALERATION =13,
	STABILITY = 14

	,NumberOfTypes
}
